let db = require('./../DB.json');
const fs = require('fs');

async function getDb(){
    return db;
}

async function setDb(dbIncoming){
    db = dbIncoming;
    fs.writeFileSync('./DB.json',JSON.stringify(db,null,4));
    return db;
}

module.exports = {
    getDb,
    setDb
}
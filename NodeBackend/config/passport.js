'use strict'
const JwtStrategy = require('passport-jwt').Strategy
const ExtractJwt = require('passport-jwt').ExtractJwt;
const { getDb } = require('./db');
// At a minimum, you must pass the `jwtFromRequest` and `secretOrKey` properties
const options = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: process.env.AUTH_SECRET
};

// app.js will pass the global passport object here, and this function will configure it
module.exports = (passport) => {
  
    // The JWT payload is passed into the verify callback
    passport.use(new JwtStrategy(options,async function(jwt_payload, done) {
        try{
            const db = await getDb();
            let user = null;
            for(let sub of db.substitutes){
              if(sub.id=== jwt_payload.user.id){
                user=sub;
              }
            }
            if(!user) return done(null,false);
            return done(null,user);            
        }catch(error){
            console.log('error: ', error);
            return done(error,false);
        }
    }));
}
'use strict'
const jsonwebtoken = require('jsonwebtoken');

async function issueJWT(user, expiresIn = '7d') {
  const payload = {
    user: {
      id: user.id,
    }
  };
  const signedToken = jsonwebtoken.sign(payload, process.env.AUTH_SECRET, { expiresIn });
  return {
    token: "Bearer " + signedToken,
    expires: expiresIn
  }
}

module.exports = {
  issueJWT
}
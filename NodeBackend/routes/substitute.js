const express = require('express');
router = express.Router();
substitute = require('../controllers/substitute');

router.get('/', substitute.getAll);
router.get('/:id', substitute.get);

module.exports = router;

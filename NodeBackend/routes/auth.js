const express = require('express');
router = express.Router();
login = require('../controllers/login');
register = require('../controllers/register');

router.post('/login', login);
router.post('/register', register);

module.exports = router;

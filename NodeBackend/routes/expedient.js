const express = require('express');
router = express.Router();
expedient = require('../controllers/expedient');

router.get('/', expedient.getAll);
router.get('/:id', expedient.get);

module.exports = router;

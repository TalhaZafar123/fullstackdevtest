const { getDb } = require("./../config/db");

module.exports = {

  async get(req, res){
    const db = await getDb();
    const result = db.substitutes.find( ({ id }) => id == req.params.id );
    res.send(result ? result : "Data not found!");
  },
  async getAll(req, res){
    const db = await getDb();
    res.send(db.substitutes);
  }

};

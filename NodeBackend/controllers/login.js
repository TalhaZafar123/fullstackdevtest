const { getDb } = require('./../config/db');
const Joi = require('joi');
const bcrypt = require('bcrypt');
const { issueJWT } = require('./../shared/utils');

userLoginSchema = Joi.object({
  email: Joi.string().email().required(),
  password: Joi.string().required()
}).required()

module.exports =  async (req, res) => {

  try{
      const userLogin= await userLoginSchema.validateAsync(req.body);

      const db = await getDb();

      let user = null;

      for(let sub of db.substitutes){
        if(sub.email.trim() === userLogin.email){
          user=sub;
        }
      }
      
      if( !user || !(await bcrypt.compare(userLogin.password,user.password))) throw new Error("Incorrect Email Address or Password"); 
      const jwtToken = await issueJWT(user);
      res.status(200).send({
          message: "Signed in succesfully",
          result: {
              data: {
                  ...jwtToken,
                  ...user
              }
          }
      });
  }catch(error){
      console.log(error.stack)
      res.status(406).send({
          message : error.message
      })
  }
};
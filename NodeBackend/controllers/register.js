const { getDb,setDb } = require('./../config/db');
const Joi = require('joi');
const bcrypt = require('bcrypt');

userAddSchema = Joi.object({
  email: Joi.string().email().required(),
  password: Joi.string().required(),
  name: Joi.string().required(),
  last_name: Joi.string().required(),
  phone: Joi.string().required(),
  image: Joi.string().required(),
  judicial_party: Joi.array().items( Joi.string().required() ).required()
}).required()

module.exports =  async (req, res) => {

  try{
      const userAdd= await userAddSchema.validateAsync(req.body);

      const db = await getDb();

      let user = null;

      for(let sub of db.substitutes){
        if(sub.email.trim() === userAdd.email){
          user=sub;
        }
      }
      if(user) throw new Error("User already exists. Try another email");
      
      userAdd.password = await bcrypt.hash(userAdd.password,10);

      db.substitutes.push({
        id: db.substitutes.length+1,
        ...userAdd
      })

      await setDb(db);

      res.status(200).send({
          message: "Signed up succesfully"
      });
  }catch(error){
      console.log(error.stack)
      res.status(406).send({
          message : error.message
      })
  }
};
const express = require('express');
app = express();
require('dotenv').config();

const passport = require('passport');
require('./config/passport')(passport);
app.use(passport.initialize());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use('/api/expedients',passport.authenticate('jwt', { session: false }), require('./routes/expedient'))
app.use('/api/substitutes',passport.authenticate('jwt', { session: false }), require('./routes/substitute'))
app.use('/api/auth', require('./routes/auth'))

const PORT = process.env.PORT || 3000;
app.listen(PORT, ()=>{
  console.log(`Listening on Port: ${PORT}`);
});
